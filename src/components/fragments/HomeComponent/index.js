import React from "react";
import { Pressable, Text, View } from "react-native";
import { VictoryPie } from "victory-native";

export default function (props) {
  const DetaiNews = () => {
    const { navigation } = props;
    navigation.navigate("News");
  };
  // const sampleData = ()=>{
  //   1
  // }
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>HomeComponent</Text>
      <View>
        <VictoryPie
          padAngle={({ datum }) => datum.y}
          innerRadius={100}
          data={[
            { x: "Cats", y: 35 },
            { x: "Dogs", y: 40 },
            { x: "Birds", y: 10 },
          ]}
        />
      </View>
      <Pressable
        onPress={() => {
          DetaiNews();
        }}
      >
        <Text>Xem Thêm</Text>
      </Pressable>
    </View>
  );
}
