import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, TextInput, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { DataTable } from "react-native-paper";
import Icon from "react-native-vector-icons/FontAwesome";
import { UseCategory } from "../../../hooks/Composable";
import { io } from "socket.io-client";
var socket = io("http://192.168.1.234:3001");
export default function CategoryComponent() {
  const { getCategory } = UseCategory();
 

  useEffect(() => {
    getCategory();
    // hanleGet()
  }, []);
  const [ listData, setListData ] = useState([]);
  const socket = io("http://192.168.1.234:3001");
  socket.emit("getData");
  socket.on("data", (data) => {
    console.log(data, "saga");
    setListData(data)
})
  return (
    // <SafeAreaView>
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.containerApp}>
        <View style={styles.containerTop}></View>
        <View style={styles.cardNoButton}>
          <Text style={styles.TitleCate}>Danh mục mã cổ phiếu</Text>
          <View style={styles.ipSearchContainer}>
            <TextInput placeholder="Tìm kiếm..." style={styles.ipSearch} />
            <Text style={styles.iconSearch}>
              <Icon name="search" size={10} color="#fff" />
            </Text>
          </View>
        </View>
        <DataTable style={styles.container}>
          <DataTable.Header style={styles.tableHeader}>
            <DataTable.Title style={{ flex: 0.8 }}>
              <Text style={{ fontSize: 12, fontWeight: "bold", color: "#fff" }}>
                Mã
              </Text>
            </DataTable.Title>
            <DataTable.Title>
              <Text style={{ fontSize: 12, fontWeight: "bold", color: "#fff" }}>
                THỊ GIÁ
              </Text>
            </DataTable.Title>
            <DataTable.Title>
              <Text style={{ fontSize: 12, fontWeight: "bold", color: "#fff" }}>
                TĂNG/GIẢM
              </Text>
            </DataTable.Title>
            <DataTable.Title style={{ flex: 0.5 }}>
              <Text style={{ fontSize: 12, fontWeight: "bold", color: "#fff" }}>
                KLGD
              </Text>
            </DataTable.Title>
            <DataTable.Title style={{ flex: 0.2 }}>
              <Text></Text>
            </DataTable.Title>
          </DataTable.Header>
          {listData.map((data, key) => {
            return (
              <DataTable.Row
                key={key}
                style={key % 2 === 0 ? styles.xyz : styles.abc}
              >
                <DataTable.Cell style={{ flex: 0.8 }}>
                  <Text style={styles.dataaaa}>{data.maCp}</Text>
                </DataTable.Cell>
                <DataTable.Cell>
                  <Text style={styles.dataaaa}> {data.thiGia} xu</Text>
                </DataTable.Cell>
                <DataTable.Cell>
                  <Text style={styles.dataaaa}> +{data.bienDo} xu</Text>
                </DataTable.Cell>
                <DataTable.Cell style={{ flex: 0.5 }}>
                  <Text style={styles.dataaaa}>{data.klgd} M</Text>
                </DataTable.Cell>
                <DataTable.Cell style={{ flex: 0.1 }}>
                  <Text>
                    <Icon name="ellipsis-v" size={20} color="#fff" />
                  </Text>
                </DataTable.Cell>
              </DataTable.Row>
            );
          })}
        </DataTable>
      </View>
    </ScrollView>
    // </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  containerApp: {
    width: "100%",
    height: 1000,
    backgroundColor: "#000000",
    paddingBottom: "20%",
  },
  TitleCate: {
    fontSize: 16,
    padding: 10,
    color: "#fff",
  },
  container: {
    marginTop: 10,
  },
  tableHeader: {
    width: "100%",
    backgroundColor: "#363636",
  },
  dataaaa: {
    textAlign: "center",
    color: "#00FF19",
    fontSize: 12,
    fontWeight: "normal",
  },
  abc: {
    border: 0,
    borderBottomColor: 0,
    backgroundColor: "#363636",
  },
  xyz: {
    color: "#fff",
    border: 0,
    borderBottomColor: 0,
    backgroundColor: "#000",
  },
  cardNoButton: {
    marginTop: 10,
    backgroundColor: "#363636",
    height: 80,
  },
  ipSearch: {
    width: "95%",
    height: 25,
    marginLeft: 11,
    backgroundColor: "black",
    color: "#fff",
    paddingLeft: 10,
    fontSize: 10,
    borderRadius: 5,
    outlineStyle: "none",
  },
  containerTop: {
    width: "100%",
    height: 50,
    backgroundColor: "#363636",
  },
  ipSearchContainer: {
    display: "flex",
    flexDirection: "row",
    position: "relative",
  },
  iconSearch: {
    position: "absolute",
    right: "5%",
    top: 6,
  },
});
