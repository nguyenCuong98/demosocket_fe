import { useDispatch, useSelector } from "react-redux";
import { authActions } from "../../actions/index";

export const UseCategory = () => {
  const dispatch = useDispatch();
  //select
  const listDataCategory = useSelector((state) => state.categorys.listData);
  const getCategory = () => dispatch(authActions.categoryRequest());
  return {
    listDataCategory,
    getCategory,
  };
};
