import { combineReducers } from "redux";
import authReducer from "./auth.reducer";
import itemCollectionReducer from "./itemReducer/item.collection.reducer";
import itemInstanceReducer from "./itemReducer/item.instance.reducer";
import categoryReducer from "./itemReducer/categoryReducer";
export default combineReducers({
  auth: authReducer,
  itemCollection: itemCollectionReducer,
  itemInstance: itemInstanceReducer,
  categorys: categoryReducer,
});
