import { actionTypes } from "@/constants";
const { authTypes } = actionTypes;
const INITIAL_STATE = {
  listData: [],
  isFetching: false,
  isError: false,
  message: "",
};
export default function categoryReducer(state = INITIAL_STATE, { type, payload }) {
   
  switch (type) {
    case authTypes.CATEGORY_REQUEST:
        return {
            ...state,
            isFetching: true
        }
    case authTypes.CATEGORY_SUCCESS:
        return {
            ...state,
            isFetching: false,
            listData: payload.listCategorys
        }
    case authTypes.CATEGORY_FAILURE:
        return {
            ...state,
            isFetching: false,
            isError: true,
            message: payload.message
        }
    default:
      return state;
  }
}
