import { REST_API_METHOD as METHOD, BASE_URL, BASE_SOCKET } from "@/constants";

const API_SCHEME = {
  // update when apply real authen api
  AUTHEN: {
    VALIDATE_TOKEN: {
      url: `${BASE_URL}/auth`,
      method: METHOD.GET,
    },
    LOGIN: {
      url: `${BASE_URL}/auth`,
      method: METHOD.GET,
    },
    LOGOUT: {
      url: `${BASE_URL}/logout`,
      method: METHOD.POST,
    },
  },
  // business api
  ITEMS: {
    CATEGORY: {
      url: `${BASE_URL}/category`,
      method: METHOD.GET,
    },
  },

  SOCKET: {
    CATEGORY_SOCKET: {
      url: `${BASE_SOCKET}/category`,
      method: METHOD.GET,
    },
  },
};

export default API_SCHEME;
