import { Helpers } from "@/utils";
import ApiScheme from "./@scheme";

export const authApi = {
  login: Helpers.createApi(ApiScheme.AUTHEN.LOGIN),
  logout: Helpers.createApi(ApiScheme.AUTHEN.LOGOUT),
  validateToken: Helpers.createApi(ApiScheme.AUTHEN.VALIDATE_TOKEN),
};

export const itemApi = {
  categoryApi: Helpers.createApi(ApiScheme.ITEMS.CATEGORY),
};

export const apiSocket = {
  categoryApiSocket: Helpers.createApi(ApiScheme.SOCKET.CATEGORY_SOCKET),
};
