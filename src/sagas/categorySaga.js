import { takeLatest, put, takeEvery } from "@redux-saga/core/effects";
import { authActions } from "@/actions";
import { actionTypes } from "@/constants";
import { itemApi } from "@/api";
import { io } from "socket.io-client";
import { useState } from "react";

function* handleCategory() {
  try {
    // const [data1, setData1] = useState(null)
    // const socket = io("http://192.168.1.234:3001");
    // socket.emit("getData");
    // socket.on("data", (data) => {
    //   console.log(data, "saga");
    // });
    put(
      authActions.categorySuccess({
        listCategorys: data,
      })
    );

    // console.log("data0", data1);

    // const res = yield itemApi.categoryApi(null, null,null);
    // yield put(
    //   authActions.categorySuccess({
    //     listCategorys: res.getData,
    //   })
    // );
  } catch (error) {
    yield put(authActions.categoryFailure(error));
  }
}
const categorySaga = [
  takeEvery(actionTypes.authTypes.CATEGORY_REQUEST, handleCategory),
];

export default categorySaga;
