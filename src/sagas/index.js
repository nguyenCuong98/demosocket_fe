import { all } from 'redux-saga/effects'
import categorySaga from './categorySaga'
import itemSaga from './item.saga'
export default function* rootSaga() {
    yield all([
        ...categorySaga,
        ...itemSaga
    ])
}