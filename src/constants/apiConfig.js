const BASE_URL = "http://192.168.1.234:3002";
const BASE_SOCKET = ""
const REST_API_METHOD = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  DELETE: "DELETE",
};

export { BASE_URL, BASE_SOCKET, REST_API_METHOD };
