import * as actionTypes from './actionTypes'
import { ORIENTATION, OS } from './device'
import { BASE_URL,BASE_SOCKET, REST_API_METHOD } from './apiConfig'

export {
    actionTypes,
    BASE_URL,
    BASE_SOCKET,
    REST_API_METHOD,
    ORIENTATION,
    OS
}